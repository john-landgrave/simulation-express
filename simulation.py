#! /usr/bin/python

# import wx
from random import random as rnd
# import time
import itertools
from numpy import random
from customer import Customer
from server import Server
from Queue import Queue
from UI import *
from math import factorial

def getValFromCDF(cdf):
	rv = rnd()
	for bucket in cdf:
		if rv <= bucket['probability']:
			return bucket['value']
	return -1

def allQueuesEmpty(queueList):
	for queue in queueList:
		if queue.qsize > 0:
			return False
	return True

def allServersIdle(serverList):
	for server in serverList:
		if not server.isBusy():
			return False
	return True

def calculateStats(customers, finalTime, simNum, numServers):
	stats = {}
	# stats['SimulationNumber'] = simNum
	stats['TotalRunTime'] = finalTime
	stats['AverageWaitTime'] = float(sum([x.getWaitTime() for x in customers]))/len(customers)
	stats['ProbabilityToWait'] = float(len([x.getWaitTime() for x in customers if x.getWaitTime() > 0]))/len(customers)
	stats['AverageServiceTime'] = float(sum([x.getServiceDuration() for x in customers]))/len(customers)
	stats['AverageInterarrivalTime'] = float(sum([x.getInterArrivalTime() for x in customers]))/len(customers)
	stats['AverageTimeInSystem'] = float(sum([x.getDepartureTime() - x.getArrivalTime() for x in customers]))/len(customers)
	for customer in customers:
		if stats.has_key('ProbabilityServer'+str(customer.getServer())+'Idle'):
			stats['ProbabilityServer'+str(customer.getServer())+"Idle"] += customer.getServiceDuration()
		else:
			stats['ProbabilityServer'+str(customer.getServer())+"Idle"] = customer.getServiceDuration()
	x = 0
	while stats.has_key('ProbabilityServer'+str(x)+'Idle'):
		stats['ProbabilityServer'+str(x)+'Idle'] = float(finalTime - stats['ProbabilityServer'+str(x)+'Idle'])/finalTime
		stats['Server'+str(x)+'Utilization'] = float(1 - stats['ProbabilityServer'+str(x)+"Idle"])
		x += 1
	rho = float(stats['AverageServiceTime'] / (numServers * stats['AverageInterarrivalTime']))
	stats['TrafficIntensity'] = rho
	stats['Throughput'] = float(len(customers) / float(finalTime))
	# Begin math for Average Queue Length and Number in System
	mu = float(1 / stats['AverageServiceTime'])
	lam = float(1 / stats['AverageInterarrivalTime'])
	probabilityL0 = ((lam/mu)**numServers)*(1/factorial(numServers))*((numServers*mu)/(numServers*mu - lam))
	Sum = 0.0
	for x in xrange(0,numServers-1):
		Sum += ((lam/mu)**x)/factorial(x)
	probabilityL0 = float(1 / (Sum + probabilityL0))
	probabilityLInf = float((((numServers*rho)**numServers)*probabilityL0)/(factorial(numServers)*(1-rho)))
	# End math for Average Queue Length and Number in System
	stats['AverageQueueLength'] = float((rho*probabilityLInf)/(1-rho)) if float((rho*probabilityLInf)/(1-rho)) > 0 else 0
	stats['AverageNumberInSystem'] = float((numServers*rho)+stats['AverageQueueLength']) if float((numServers*rho)+stats['AverageQueueLength']) > 0 else 0
	return stats

def simulate(numberOfCustomers=50, numberOfQueues=3, numberOfServers=3, CDF=[0.1, 0.3, 0.6, 0.85, 0.95, 1.0], vals = [1, 2, 3, 4, 5, 6], pullFromRandomQueueIfEmpty = False, interArrivalTimeMin=1, interArrivalTimeMax=5, simNumber=0):

	# Initialization, these should be setable via the UI
	if not len(CDF) == len(vals):
		return 1;
	serviceTimeDistribution = [{'probability': x, 'value': y} for x,y in zip(CDF, vals)]
	interArrivalTime = {'min': interArrivalTimeMin, 'max': interArrivalTimeMax}

	# Variables that should be used for keeping track of simulation progression
	queueList = []
	customerList = []
	serverList = []
	doneCustomerList = []
	currentTime = 0
	lastArrivalTime = 0

	# numberOfServers >= numberOfQueues
	if numberOfServers < numberOfQueues:
		numberOfServers = numberOfQueues

	# Simulation Setup (Customer/Server/Queue Creation)
	for x in xrange(0, numberOfQueues):
		queueList.append(Queue(numberOfCustomers))

	for x in xrange(0, numberOfServers):
		serverList.append(Server(x))

	for x in xrange(0, numberOfCustomers):
		newCustomer = Customer()
		newCustomer.setCustomerId(x)
		newCustomer.setInterArrivalTime(random.randint(interArrivalTime['min'], interArrivalTime['max']+1))
		newCustomer.setServiceDuration(getValFromCDF(serviceTimeDistribution))
		customerList.append(newCustomer)

	# Start simulation loop
	while len(doneCustomerList) != numberOfCustomers:
		# Determine whether or not new Customer should arrive
		# Customer should arrive if the lastArrivalTime - currentTime >= the next customer's interarrival time
		# Customer should arrive if this is the start of the simulation (currentTime == 0)
		if (len(customerList) > 0) and ((currentTime - lastArrivalTime) >= customerList[0].getInterArrivalTime()) or (currentTime == 0):
			newCustomer = customerList[0]
			newCustomer.setArrivalTime(currentTime)
			# Determine which Queue to put the customer in
			queueList[random.randint(len(queueList))].put(newCustomer)
			customerList.pop(0)
			lastArrivalTime = currentTime

		# Determine whether a customer should be served
		# Case where the number of servers is equal to the nubmer of queues, and each server pulls from their own queue only
		for server, queue in itertools.izip_longest(serverList, queueList):
			if queue == None:
				queue = queueList[random.randint(len(queueList))]

			if pullFromRandomQueueIfEmpty and queue.qsize==0 and not allQueuesEmpty():
				while queue.qsize==0:
					queue = queueList[random.randint(len(queueList))]

			# Clean up completed services
			if server.isServiceComplete():
				customer = server.getClient()
				server.finishService()
				customer.setServiceStopTime(currentTime)
				customer.setDepartureTime(currentTime)
				doneCustomerList.append(customer)
			
			# If the server is not busy and there is someone in their queue, they should serve them
			if (not server.isBusy()) and (queue.qsize() > 0):
				customer = queue.get()
				server.setNewService(customer.getServiceDuration(), customer)
				customer.setServiceStartTime(currentTime)
				customer.setServer(server.getId())
			# If there are no clients to serve, the server is idle
			elif queue.qsize() == 0:
				server.incrementIdleTime()
			
			# If the server is serving a client, increment that service time
			if server.isBusy():
				server.incrementServiceTime()

		currentTime += 1

	# Calculate stats and output stuff
	stats = calculateStats(doneCustomerList, currentTime-1, simNumber, numberOfServers)
	customerOutput = []
	for customer in doneCustomerList:
		customerOutput.append(customer.dumpArray())

	return stats, customerOutput

if __name__ == "__main__":
	app = wx.App()
	MainFrame().Show()
	app.MainLoop()
