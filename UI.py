import re
import wx
import wx.grid
import wx.lib.scrolledpanel as scrolled
from simulation import simulate
import wx.lib.agw.foldpanelbar as fpb

myEVT_SETTINGS_UPDATE = wx.NewEventType()
EVT_SETTINGS_UPDATE = wx.PyEventBinder(myEVT_SETTINGS_UPDATE, 1)

def FireSettingsUpdateEvent(obj, id, val, attr):
	event = SettingsUpdateEvent(myEVT_SETTINGS_UPDATE, id, val, attr)
	wx.PostEvent(obj.GetEventHandler(), event)

class SettingsUpdateEvent(wx.PyCommandEvent):

	def __init__(self, evtType, id, val=None, attr=None):
		wx.PyCommandEvent.__init__(self, evtType, id)
		self.Value = val
		self.Attribute = attr

	def GetValue(self):
		return self.Value

	def GetAttribute(self):
		return self.Attribute

myEVT_RESULTS_UPDATE = wx.NewEventType()
EVT_RESULTS_UPDATE = wx.PyEventBinder(myEVT_RESULTS_UPDATE, 2)

class ResultsUpdateEvent(wx.PyCommandEvent):
	def __init__(self, evtType, id, customerInfo=None, stats=None):
		wx.PyCommandEvent.__init__(self, evtType, id)
		self.CustomerInfo = customerInfo
		self.Stats = stats

	def GetCustomerInfo(self):
		return self.CustomerInfo

	def GetStats(self):
		return self.Stats

class SettingsPage(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		self.SetStyle = wx.RAISED_BORDER

		gbSizer = wx.GridBagSizer(5,5)

		# === Start System Settings section
		SystemSettingsBox = wx.StaticBox(self, -1, 'System Settings')
		SystemSettingsPanelSizer = wx.StaticBoxSizer(SystemSettingsBox, wx.VERTICAL)
		SystemSettingsPanelContentsGBSizer = wx.GridBagSizer()

		# Number of Servers Spinner
		SystemSettingsPanelContentsGBSizer.Add(wx.StaticText(self, 102, "Number of Servers", style=wx.ALIGN_LEFT), pos=wx.GBPosition(0,0), span=wx.GBSpan(1,1))
		numServers = wx.SpinCtrl(self, 103)
		numServers.Bind(wx.EVT_SPINCTRL, self.updateNumServers)
		SystemSettingsPanelContentsGBSizer.Add(numServers, pos=wx.GBPosition(1,0), span=wx.GBSpan(1,1))

		# Number of Queues Spinner
		SystemSettingsPanelContentsGBSizer.Add(wx.StaticText(self, 104, "Number of Queues", style=wx.ALIGN_LEFT), pos=wx.GBPosition(0,1), span=wx.GBSpan(1,1))
		numQueues = wx.SpinCtrl(self, 105)
		numQueues.Bind(wx.EVT_SPINCTRL, self.updateNumQueues)
		SystemSettingsPanelContentsGBSizer.Add(numQueues, pos=wx.GBPosition(1,1), span=wx.GBSpan(1,1))

		# Queue-Pull checkbox
		randomCheckbox = wx.CheckBox(self, 300, "Should Servers pull from another queue if theirs is empty?")
		randomCheckbox.Bind(wx.EVT_CHECKBOX, self.updateRandomPull)
		SystemSettingsPanelContentsGBSizer.Add(randomCheckbox, pos=wx.GBPosition(1,2), span=wx.GBSpan(1,3))

		# Number of Runs input
		SystemSettingsPanelContentsGBSizer.Add(wx.StaticText(self, 106, "Number of Runs", style=wx.ALIGN_LEFT), pos=wx.GBPosition(2,0), span=wx.GBSpan(1,1))
		numRuns = wx.SpinCtrl(self, 107)
		numRuns.Bind(wx.EVT_SPINCTRL, self.updateNumRuns)
		SystemSettingsPanelContentsGBSizer.Add(numRuns, pos=wx.GBPosition(3,0), span=wx.GBSpan(1,1))

		SystemSettingsPanelSizer.Add(SystemSettingsPanelContentsGBSizer, 0, wx.ALL|wx.CENTER, 5)
		gbSizer.Add(SystemSettingsPanelSizer, pos=wx.GBPosition(0,0), span=wx.GBSpan(3,5))
		#  === End System Settings Section

		#  === Start Client Settings section
		ClientSettingsBox = wx.StaticBox(self, -1, "Client Settings")
		ClientSettingsBoxSizer = wx.StaticBoxSizer(ClientSettingsBox, wx.VERTICAL)
		ClientSettingsBoxContentsGBSizer = wx.GridBagSizer()

		# Number of Customers Spinner
		ClientSettingsBoxContentsGBSizer.Add(wx.StaticText(self, 100, "Number of Customers", style=wx.ALIGN_LEFT), pos=wx.GBPosition(0,0), span=wx.GBSpan(1,1))
		numCustomers = wx.SpinCtrl(self, 101)
		self.Bind(wx.EVT_SPINCTRL, self.updateNumCustomers, numCustomers)
		ClientSettingsBoxContentsGBSizer.Add(numCustomers, pos=wx.GBPosition(1,0), span=wx.GBSpan(1,1))

		# InterArrival Times heading
		ClientSettingsBoxContentsGBSizer.Add(wx.StaticText(self, 102, "Interarrival Time Parameters", style=wx.ALIGN_LEFT), pos=wx.GBPosition(3,0), span=wx.GBSpan(1,1))

		# Minimum Inter-Arrival Time Spinner
		ClientSettingsBoxContentsGBSizer.Add(wx.StaticText(self, 200, "Minimum", style=wx.ALIGN_LEFT), pos=wx.GBPosition(4,0), span=wx.GBSpan(1,2))
		minInterArrival = wx.SpinCtrl(self, 201)
		minInterArrival.Bind(wx.EVT_SPINCTRL, self.updateMinInterArrival)
		ClientSettingsBoxContentsGBSizer.Add(minInterArrival, pos=wx.GBPosition(5,0), span=wx.GBSpan(1,2))

		# Maximum Inter-Arrival Time Spinner
		ClientSettingsBoxContentsGBSizer.Add(wx.StaticText(self, 202, "Maximum", style=wx.ALIGN_LEFT), pos=wx.GBPosition(4,2), span=wx.GBSpan(1,2))
		maxInterArrival = wx.SpinCtrl(self, 203)
		maxInterArrival.Bind(wx.EVT_SPINCTRL, self.updateMaxInterArrival)
		ClientSettingsBoxContentsGBSizer.Add(maxInterArrival, pos=wx.GBPosition(5,2), span=wx.GBSpan(1,2))

		ClientSettingsBoxSizer.Add(ClientSettingsBoxContentsGBSizer, 0, wx.ALL|wx.CENTER, 5)
		gbSizer.Add(ClientSettingsBoxSizer, pos=wx.GBPosition(4,0), span=wx.GBSpan(6,5))
		#  === End Client Settings Section

		self.SetSizer(gbSizer)

	def updateNumCustomers(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "NumCustomers")

	def updateNumServers(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "NumServers")

	def updateNumQueues(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "NumQueues")

	def updateMinInterArrival(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "MinInterArrival")

	def updateMaxInterArrival(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "MaxInterArrival")

	def updateNumRuns(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.GetEventObject().GetValue(), "NumRuns")

	def updateRandomPull(self, event):
		FireSettingsUpdateEvent(self, self.GetId(), event.IsChecked(), "RandomPullCheckbox")

class ResultsPage(scrolled.ScrolledPanel):
	def __init__(self, parent):
		scrolled.ScrolledPanel.__init__(self, parent, -1)
		self.SetStyle = wx.SUNKEN_BORDER

		self.Sizer = wx.BoxSizer(wx.VERTICAL)

		self.SetSizer(self.Sizer)
		self.SetAutoLayout(1)
		self.SetupScrolling()

	def UpdateResults(self, event):
		CustomerInfo = event.GetCustomerInfo()
		Stats = event.GetStats()

		if hasattr(self, "PanelBar"):
			self.Sizer.Remove(self.PanelBar)

		self.PanelBar = fpb.FoldPanelBar(self, -1, agwStyle=fpb.FPB_VERTICAL|fpb.FPB_SINGLE_FOLD, style=wx.EXPAND, size=(-1,400))
		self.Panels = []

		if (len(CustomerInfo) != len(Stats)):
			wx.MessageBox(message="There's been a problem with the simulation resultset", caption='Error', style=wx.OK | wx.ICON_HAND)

		if (len(self.Panels) > 0):
			for panel in self.Panels:
				del panel

		for x in xrange(0,len(CustomerInfo)):
			newPanel = self.PanelBar.AddFoldPanel("Results - "+str(x+1))
			newPanel.SetSize((400,400))
			newPanel.SetAutoLayout(1)
			self.Panels.append(newPanel)
			resultsWindow = ResultsWindow(newPanel, CustomerInfo[x], Stats[x])
			resultsWindow.SetSize((400,400))
			self.PanelBar.AddFoldPanelWindow(newPanel, resultsWindow)
		
		self.Sizer.Add(self.PanelBar, 1, wx.EXPAND|wx.LEFT|wx.RIGHT, border=20)
		self.SetAutoLayout(1)
		self.SetupScrolling()

class ResultsWindow(scrolled.ScrolledPanel):
	def __init__(self, parent, customerInfo=None, stats=None):
		scrolled.ScrolledPanel.__init__(self, parent, -1)
		self.SetStyle = wx.SUNKEN_BORDER

		self.CustomerInfo = customerInfo
		self.Stats = stats

		self.grid = wx.grid.Grid(self)

		self.grid.CreateGrid(0,9)
		self.grid.SetColMinimalAcceptableWidth(70)

		columns = [
			"Customer Id", 80,
			"Server Id", 80,
			"Inter-Arrival Time", 80,
			"Arrival Time", 60,
			"Service Start Time", 80,
			"Wait Time", 80,
			"Service Stop Time", 80,
			"Departure Time", 80,
			"Service Duration", 80
		]

		for x in xrange(0, len(columns),2):
			self.CreateColumn(int(x/2), columns[x], columns[x+1])

		self.StatsGrid = wx.grid.Grid(self)
		self.StatsGrid.CreateGrid(0,1)
		self.StatsGrid.SetColMinimalAcceptableWidth(140)

		self.StatsGrid.SetColLabelValue(0, "")

		self.PopulateTable()

		OutputsBox = wx.StaticBox(self, -1, "Output Results")
		OutputsBoxSizer = wx.StaticBoxSizer(OutputsBox, wx.VERTICAL)
		CSVButton = wx.Button(self, label="Output to CSV")
		CSVButton.Bind(wx.EVT_BUTTON, self.OutputToCSV)
		OutputsBoxSizer.Add(CSVButton, 1, wx.EXPAND, 10)

		self.sizer = wx.BoxSizer(wx.HORIZONTAL)
		self.grid.AutoSize()
		self.StatsGrid.AutoSize()
		self.sizer.Add(self.grid)
		vSizer = wx.BoxSizer(wx.VERTICAL)
		vSizer.Add(self.StatsGrid)
		vSizer.Add(OutputsBoxSizer)
		self.sizer.Add(vSizer)
		self.SetSizer(self.sizer)
		self.SetAutoLayout(1)
		self.SetupScrolling()

	def CreateColumn(self, index=-1, name="MISSING_NAME", minWidth=60):
		self.grid.SetColLabelValue(index, name)
		self.grid.SetColMinimalWidth(index, minWidth)

	def CreateRow(self, index=-1, name="MISSING_NAME"):
		self.StatsGrid.SetRowLabelValue(index, name)
		self.StatsGrid.SetRowLabelSize(180)

	def PopulateTable(self):
		if (self.grid.GetNumberRows() > 0):
			self.grid.DeleteRows(0, self.grid.GetNumberRows())
		if (self.grid.InsertRows(0,len(self.CustomerInfo))):
			self.grid.AutoSize()
			for row in xrange(0,len(self.CustomerInfo)):
				for col in xrange(0,9):
					self.grid.SetReadOnly(row, col)
					self.grid.SetCellValue(row, col, self.CustomerInfo[row][col])

		if (self.StatsGrid.GetNumberRows() > 0):
			self.StatsGrid.DeleteRows(0, self.StatsGrid.GetNumberRows())
		if (self.StatsGrid.InsertRows(0, len(self.Stats))):
			self.StatsGrid.AutoSize()
			index = 0
			for key, value in self.Stats.iteritems():
				self.CreateRow(index, self.CamelToSpaces(key))
				self.StatsGrid.SetReadOnly(index, 0)
				self.StatsGrid.SetCellValue(index, 0, str(self.Stats[key]))
				index += 1

		self.SetAutoLayout(1)
		self.SetupScrolling()

	def CamelToSpaces(self, camelCase="InputMissing"):
		return re.sub(r'((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))', r' \1', camelCase)

	def OutputToCSV(self, event):
		# Error handling to make sure that a sim has been run.
		try:
			self.CustomerInfo
		except AttributeError:
			wx.MessageBox(message="Customer info is missing. Have you run a simulation?", caption='Error', style=wx.OK | wx.ICON_HAND)
			return
		try:
			self.Stats
		except AttributeError:
			wx.MessageBox(message="Stats are missing, have you run a simulation?", caption='Error', style=wx.OK | wx.ICON_HAND)
			return

		SaveFileDialog = wx.FileDialog(self, "Save Results as CSV", "", "", "CSV Files (*.csv)|*.csv", wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)

		if (SaveFileDialog.ShowModal() == wx.ID_CANCEL):
			return

		OutputPath = SaveFileDialog.GetPath()
		if (OutputPath[-4:] != ".csv"):
			OutputPath = OutputPath+".csv"

		with open(OutputPath, 'w') as f:
			f.write("Customer Id, Server Id, Inter-Arrival Time, Arrival Time, Service Start Time, Wait Time, Service Stop Time, Departure Time, Service Duration\n")
			for customer in self.CustomerInfo:
				f.write(','.join([str(x) for x in customer])+'\n')
			f.write(','.join([str(x) for x in self.Stats.iterkeys()])+'\n')
			f.write(','.join([str(y) for x,y in self.Stats.iteritems()])+'\n')

class ServiceTimePage(scrolled.ScrolledPanel):
	def __init__(self, parent):
		scrolled.ScrolledPanel.__init__(self, parent, -1)
		self.SetStyle = wx.SUNKEN_BORDER

		self.grid = wx.grid.Grid(self)

		self.grid.CreateGrid(20,3)
		self.grid.SetColMinimalAcceptableWidth(70)
		# Setup column 1
		self.grid.SetColLabelValue(0, "Service Time")
		self.grid.SetColMinimalWidth(0, 60)

		# Setup column 2
		self.grid.SetColLabelValue(1, "Probability (0.x)")
		self.grid.SetColMinimalWidth(0, 80)

		# Setup column 2
		self.grid.SetColLabelValue(2, "Cumulative Probability")
		self.grid.SetColMinimalWidth(0, 80)

		for x in xrange(0,20):
			self.grid.SetReadOnly(x,2)

		self.grid.Bind(wx.grid.EVT_GRID_CELL_CHANGE, self.OnGridCellChange)

		sizer = wx.GridBagSizer(1,1)
		self.grid.AutoSize()
		sizer.Add(self.grid, pos=wx.GBPosition(1,1), span=wx.GBSpan(1,1))
		self.SetSizer(sizer)
		self.SetAutoLayout(1)
		self.SetupScrolling()

	def OnGridCellChange(self, event):
		Row = event.GetRow()
		Col = event.GetCol()

		Sum = 0
		CDF = []
		CDFVals = []
		for x in xrange(0, 20):
			if self.grid.GetCellValue(x, 1) != '':
				Sum += float(self.grid.GetCellValue(x, 1))
				self.grid.SetCellValue(x, 2, str(Sum))
				if Sum > 1.0:
					self.grid.SetCellBackgroundColour(x, 2, wx.Colour(255,0,0,.5))
				elif Sum == 1.0:
					self.grid.SetCellBackgroundColour(x, 2, wx.Colour(0,255,0, 0.5))
				else:
					self.grid.SetCellBackgroundColour(x, 2, wx.Colour(255,255,255,1.0))

				thisCDF = float(self.grid.GetCellValue(x, 2))
				thisCDFVal = float(self.grid.GetCellValue(x, 0))

				CDF.append(thisCDF)
				CDFVals.append(thisCDFVal)
			else:
				break

		FireSettingsUpdateEvent(self, self.GetId(), float(Sum), "TotalServiceTime")
		FireSettingsUpdateEvent(self, self.GetId(), CDF, "CDF")
		FireSettingsUpdateEvent(self, self.GetId(), CDFVals, "CDFVals")

		event.Skip()

class NotebookPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		self.SetStyle = wx.SUNKEN_BORDER

		self.notebookCtrl = wx.Notebook(self)
		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(self.notebookCtrl, 1, wx.EXPAND)
		self.SetSizer(sizer)

	def AddNotebookPage(self, page=None, name="MISSING_NAME"):
		self.notebookCtrl.AddPage(page, name)

	def GetNotebookControl(self):
		return self.notebookCtrl

class ButtonPanel(wx.Panel):
	Parent = None

	def __init__(self, parent):
		wx.Panel.__init__(self, parent)
		self.SetStyle = wx.SUNKEN_BORDER
		self.Parent = parent

		button = wx.Button(self, label="Start Simulation")
		button.Bind(wx.EVT_BUTTON, self.onSimulationStartButtonClick)
		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(button, 1, wx.EXPAND, 10)
		self.SetSizer(sizer)

	def onSimulationStartButtonClick(self, event):
		Settings = self.Parent.Settings

		SettingsTranslationDict = {
			'NumCustomers' : "Number of Customers",
			'NumServers' : "Number of Servers",
			'NumQueues' : "Number of Queues",
			'MinInterArrival' : "Minimum Interarrival Time",
			'MaxInterArrival' : "Maximum Interarrival Time",
			'TotalServiceTime' : "Cumulative Probability for the Service Time Distribution",
			'NumRuns' : "Number of Runs"
		}

		ErrorMessage = ""
		for key, value in Settings.iteritems():
			if key == "TotalServiceTime":
				if float(value) != 1.0:
					ErrorMessage += "- The {0} must equal 1.\n".format(SettingsTranslationDict[key])
			if (key != "CDF") and (key != "CDFVals") and (key != "RandomPullCheckbox"):
				if float(value) <= 0:
					ErrorMessage += "- The {0} must be greater than or equal to 1.\n".format(SettingsTranslationDict[key])


		if ErrorMessage == "":
			self.Parent.startSimulation();
		else:
			wx.MessageBox(message=ErrorMessage, caption='Error', style=wx.OK | wx.ICON_HAND)


class MainFrame(wx.Frame):

	TotalServiceTime = 0
	NumCustomers = 0
	NumServers = 0
	NumQueues = 0
	NumRuns = 0
	MinInterArrival = 0
	MaxInterArrival = 0
	CDF = []
	CDFVals = []
	RandomPullCheckbox = False
	Stats = None
	CustomerOutput = None

	def __init__(self):
		wx.Frame.__init__(self, None, title="Simulation Xpress", size=wx.Size(700,700))

		buttonPanel = ButtonPanel(self)
		# Create NotebookPanel
		notebookPanel = NotebookPanel(self)
		# Create Pages for Notebook Control
		settingsPage = SettingsPage(notebookPanel.GetNotebookControl())
		resultsPage = ResultsPage(notebookPanel.GetNotebookControl())
		serviceTimePage = ServiceTimePage(notebookPanel.GetNotebookControl())

		sizer = wx.BoxSizer(wx.VERTICAL)
		sizer.Add(notebookPanel, 5, wx.EXPAND)
		sizer.Add(buttonPanel, 1, wx.EXPAND)
		self.SetSizer(sizer)

		self.Settings = {
			'NumCustomers' : self.NumCustomers,
			'NumServers' : self.NumServers,
			'NumQueues' : self.NumQueues,
			'MinInterArrival' : self.MinInterArrival,
			'MaxInterArrival' : self.MaxInterArrival,
			'CDF' : self.CDF,
			'CDFVals' : self.CDFVals,
			'TotalServiceTime' : self.TotalServiceTime,
			'RandomPullCheckbox' : self.RandomPullCheckbox,
			"NumRuns" : self.NumRuns
		}

		self.Bind(EVT_SETTINGS_UPDATE, self.UpdateSettings)

		# Configure pages for Notebook Control
		notebookPanel.AddNotebookPage(settingsPage, "Main Settings")
		notebookPanel.AddNotebookPage(serviceTimePage, "Service Time Probabilities")
		notebookPanel.AddNotebookPage(resultsPage, "Results")
		self.Bind(EVT_RESULTS_UPDATE, resultsPage.UpdateResults)

	def UpdateSettings(self, event):
		attr = event.GetAttribute()
		val = event.GetValue()
		self.Settings[attr] = val

	def startSimulation(self):
		self.Stats, self.CustomerOutput = [],[]
		for x in xrange(0,int(self.Settings['NumRuns'])):
			stats, customerOutput = simulate(self.Settings['NumCustomers'],
													   self.Settings['NumQueues'],
													   self.Settings['NumServers'],
													   self.Settings['CDF'],
													   self.Settings['CDFVals'],
													   self.Settings['RandomPullCheckbox'],
													   self.Settings['MinInterArrival'],
													   self.Settings['MaxInterArrival'],
													   self.Settings['NumRuns'])
			self.Stats.append(stats)
			self.CustomerOutput.append(customerOutput)

		event = ResultsUpdateEvent(myEVT_RESULTS_UPDATE, self.GetId(), self.CustomerOutput, self.Stats)
		wx.PostEvent(self.GetEventHandler(), event)
