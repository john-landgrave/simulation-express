class Customer():
	_customerId = -1
	_arrivalTime = -1
	_serviceStartTime = -1
	_serviceStopTime = -1
	_waitTime = -1
	_departureTime = -1
	_interArrivalTime = -1
	_serviceDuration = -1
	_server = -1

	def setInterArrivalTime(self, time):
		# print "Setting Interarrival Time to {}".format(time)
		self._interArrivalTime = time

	def getInterArrivalTime(self):
		return self._interArrivalTime

	def setCustomerId(self, newId):
		self._customerId = newId

	def setArrivalTime(self, arrivalTime):
		# print "Setting Arrival Time to {}".format(arrivalTime)
		self._arrivalTime = arrivalTime

	def getArrivalTime(self):
		return self._arrivalTime

	def setServiceStartTime(self, startTime):
		# print "Setting Service Start Time to {}".format(startTime)
		self._serviceStartTime = startTime

	def getServiceStartTime(self):
		return self._serviceStartTime

	def setServiceStopTime(self, stopTime):
		# print "Setting Service Stop Time to {}".format(stopTime)
		self._serviceStopTime = stopTime
		self._waitTime = self._serviceStartTime - self._arrivalTime

	def getServiceStopTime(self):
		return self._serviceStopTime

	def setDepartureTime(self, departureTime):
		self._departureTime = departureTime

	def getDepartureTime(self):
		return self._departureTime

	def setServiceDuration(self, duration):
		# print "Setting Service Duration to {}".format(duration)
		self._serviceDuration = duration

	def getServiceDuration(self):
		return self._serviceDuration

	def getWaitTime(self):
		return self._waitTime

	def setServer(self, serverId):
		self._server = serverId

	def getServer(self):
		return self._server

	def dumpArray(self):
		return [str(self._customerId), str(self._server), str(self._interArrivalTime), str(self._arrivalTime), str(self._serviceStartTime), str(self._waitTime), str(self._serviceStopTime), str(self._departureTime), str(self._serviceDuration)]

	def dumpCSV(self):
		return ','.join([str(self._customerId), str(self._server), str(self._interArrivalTime), str(self._arrivalTime), str(self._serviceStartTime), str(self._waitTime), str(self._serviceStopTime), str(self._departureTime), str(self._serviceDuration)])

	def dumpJSON(self):
		return '{"CustomerID": %d, "ServerID": %d, "ArrivalTime": %d, "ServiceStartTime": %d, "ServiceStopTime": %d, "WaitTime": %d, "DepartureTime": %d, "ServiceDuration": %d}' % (self._customerId, self._server, self._arrivalTime, self._serviceStartTime, self._serviceStopTime, self._waitTime, self._departureTime, self._serviceDuration)