class Server():
	_serverId = -1
	_isBusy = False
	_idleTime = 0
	_currentServiceTime = -1
	_currentServiceDuration = -1
	_currentClient = None

	def __init__(self, id):
		self._serverId = id

	def isBusy(self):
		return self._isBusy

	def getId(self):
		return self._serverId

	def _toggleBusy(self):
		self._isBusy = not self._isBusy

	def incrementServiceTime(self):
		self._currentServiceTime += 1

	def incrementIdleTime(self):
		if not self._isBusy:
			self._idleTime += 1

	def isServiceComplete(self):
		return self._currentClient != None and (self._currentServiceTime >= self._currentServiceDuration)

	def setNewService(self, duration, client):
		self._currentServiceDuration = duration
		self._currentClient = client
		self._toggleBusy()

	def finishService(self):
		self._currentServiceTime = 0
		self._currentClient = None
		self._toggleBusy()

	def getClient(self):
		return self._currentClient